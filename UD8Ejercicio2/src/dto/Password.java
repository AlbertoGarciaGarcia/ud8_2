package dto;

public class Password {

	private int longitud;
	private String contraseņa; // Creamos los atributos
	
	
	public Password() {
		this.longitud=8; // Asignamos el valor default
		this.contraseņa=" ";
	}
	
	public Password(int longitud) {
		this.longitud=longitud;
		this.contraseņa=generarPassword(longitud); // Llamamos a la funcion que hemos hecho para generar la contraseņa
	}
	
	
	
	@Override
	public String toString() {
		return "Password [longitud=" + longitud + ", contraseņa=" + contraseņa + "]"; // Hacemos el toString
	}

	public static String generarPassword(int longitud) {
        int numero;
        		char[] array = new char[longitud];
        		for (int i = 0; i < longitud; i++) { // Durante la longitud introducida por parametros
        			numero = (int) (Math.random() * (122 - 48 + 1) + 48); // Hacemos que numero sea un caracter aleatorio generado ascii
            array[i] = (char) numero; // Lo introducimos en un array
        }
        return String.copyValueOf(array); // Y devolvemos el array
    }
	
	
	
}
